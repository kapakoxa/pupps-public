file {'/tmp/example_file.txt':
  ensure => file,
  content => 'Sample file \n',
  owner => 'vagrant',
  group => 'vagrant',
  mode => '0755',
}
